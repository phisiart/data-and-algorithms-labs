﻿#ifndef _TREE_H
#define _TREE_H

#include <iostream>
#include <cstdio>

template <class Type> class TreeNode;

template <class Type> class Tree;

template <class Type>
std::ostream& operator<<(std::ostream& os, const Tree<Type>& tree);

template <class Type>
std::ostream& operator<<(std::ostream& os, const TreeNode<Type>& node);

template <class Type>
class TreeNode {
    friend std::ostream& operator<< <Type>(std::ostream& os, const TreeNode& node);
public:
    TreeNode() : subtree_scale(1), depth(1), left(NULL), right(NULL) {}
    TreeNode(const Type& value) : subtree_scale(1), depth(1), value(value), left(NULL), right(NULL) {}
    Type value;
    size_t subtree_scale;
    size_t depth;
    TreeNode* left;
    TreeNode* right;
};

void EmptyFunc() {}

template <class Type>
class Tree {
    friend std::ostream& operator<< <Type>(std::ostream& os, const Tree& tree);

public:
    enum InsertTo {
        LEFT,
        RIGHT,
        ROOT
    };

    typedef bool (*compare_t)(const Type&, const Type&);
    typedef void (*node_func_t)(const TreeNode<Type>&);
    typedef void (*void_t)();

    // 树的构造函数必须提供比较函数 smaller_than
    Tree(compare_t smaller_than) : smaller_than(smaller_than), root(NULL), num_elements(0) {}

    // 清空 node 及其整个子树
    void _free(TreeNode<Type>*& node) {
        if (node == NULL) {
            return;
        }
        _free(node->left);
        _free(node->right);
        delete node;
        node = NULL;
    }

    // 清空整棵树
    void free() {
        _free(root);
    }

    // 析构时必须清空整棵树
    ~Tree() { free(); }

    size_t GetDepth(TreeNode<Type>* node) {
        if (node != NULL) {
            return node->depth;
        } else {
            return 0;
        }
    }

    // AVL 插入元素，保证子树平衡，而且 depth 值正确
    void AVLInsert(const Type& value, TreeNode<Type>*& node) {

        // 1. 若此节点为空，那么就顺势插入
        if (node == NULL) {
            node = new TreeNode<Type>(value);
            return;
        }

        // 2. 若此元素小于当前位置，就应该插到左边
        if (smaller_than(value, node->value)) {
            AVLInsert(value, node->left);

            // 有可能出现不平衡，因为左子树更高了
            if (GetDepth(node->left) - GetDepth(node->right) == 2) {
                // 这时分两种情况
                // 1) 元素插在左左
                if (smaller_than(value, node->left->value)) {
                    LLRotate(node);
                } else { // 2) 元素插在左右
                    LRRotate(node);
                }
            }

        } else { // 3. 若此元素大于等于当前位置，就应该插到右边
            AVLInsert(value, node->right);

            // 有可能出现不平衡，因为右子树更高了
            if (GetDepth(node->right) - GetDepth(node->left) == 2) {
                // 这时分两种情况
                // 1) 元素插在右左
                if (smaller_than(value, node->right->value)) {
                    RLRotate(node);
                } else { // 2) 元素插在右右
                    RRRotate(node);
                }
            }
        }

        node->depth = (GetDepth(node->left) > GetDepth(node->right) ? GetDepth(node->left) : GetDepth(node->right)) + 1;

    }

    // AVL 删除元素，保证子树平衡，而且 depth 值正确
    void AVLDelete(const Type& value, TreeNode<Type>*& node) {

        // 如果都查找完了还没找到，那就不删除
        if (node == NULL) {
            return;
        }

        // 1. 如果 value 在左子树
        if (smaller_than(value, node->value)) {
            AVLDelete(value, node->left);

            // 此时可能出现不平衡，即左子树太矮
            if (GetDepth(node->right) - GetDepth(node->left) == 2) {
                if (GetDepth(node->right->right) > GetDepth(node->right->left)) {
                    RRRotate(node);
                } else {
                    RLRotate(node);
                }
            }

        } else if (value == node->value) { // 2. 如果找到了
            // 1) 这个节点没儿子，太好了
            if (node->left == NULL && node->right == NULL) {
                delete node;
                node = NULL;
                return;
            }

            // 2) 这个节点只有左儿子，这也不错
            if (node->right == NULL) {
                TreeNode<Type>* temp = node;
                node = node->left;
                delete temp;
                return;
            }

            // 3) 这个节点只有右儿子，这也不错
            if (node->left == NULL) {
                TreeNode<Type>* temp = node;
                node = node->right;
                delete temp;
                return;
            }

            // 4) 这个节点即有左儿子又有右儿子，这就不好办了
            // 首先找到右子树中的最小值
            TreeNode<Type>* temp = node->right;
            while (temp->left != NULL) {
                temp = temp->left;
            }
            Type _value = temp->value;

            // 将这个最小值删除
            AVLDelete(_value, node->right);

            // 然后用这个值替换当前值
            node->value = _value;

            // 此时可能出现不平衡，即右子树太矮
            if (GetDepth(node->left) - GetDepth(node->right) == 2) {
                if (GetDepth(node->left->right) > GetDepth(node->left->left)) {
                    LRRotate(node);
                } else {
                    LLRotate(node);
                }
            }

        } else { // 3. 如果 value 在右子树
            AVLDelete(value, node->right);

            // 此时可能出现不平衡，即右子树太矮
            if (GetDepth(node->left) - GetDepth(node->right) == 2) {
                if (GetDepth(node->left->right) > GetDepth(node->left->left)) {
                    LRRotate(node);
                } else {
                    LLRotate(node);
                }
            }
        }

        node->depth = (GetDepth(node->left) > GetDepth(node->right) ? GetDepth(node->left) : GetDepth(node->right)) + 1;

    }

    // 在树中插入元素，递归操作，自动更新深度
    void _insert(const Type& value, TreeNode<Type>* node) {

        if (root == NULL) {
            root = new TreeNode<Type>(value);
            ++num_elements;
            return;
        }

        ++(node->subtree_scale);

        if (smaller_than(value, node->value)) {
            // insert to left

            if (node->left == NULL) {
                // insert here
                node->left = new TreeNode<Type>(value);
                ++num_elements;

                // 若右边也为空，则说明深度增加了
                if (node->right == NULL) {
                    ++node->depth;
                }

            } else {
                // recursion
                _insert(value, node->left);

                if (node->right == NULL) {
                    node->depth = node->left->depth + 1;
                } else {
                    node->depth = (
                                      (node->left->depth > node->right->depth)
                                      ? node->left->depth
                                      : node->right->depth
                                  ) + 1;
                }

            }

        } else {
            // insert to right

            if (node->right == NULL) {
                // insert here
                node->right = new TreeNode<Type>(value);
                ++num_elements;

                // 若左边也为空，则说明深度增加了
                if (node->left == NULL) {
                    ++node->depth;
                }

            } else {
                // recursion
                _insert(value, node->right);

                if (node->left == NULL) {
                    node->depth = node->right->depth + 1;
                } else {
                    node->depth = (
                        (node->left->depth > node->right->depth)
                        ? node->left->depth
                        : node->right->depth
                        ) + 1;
                }
            }
        }
    }

    // 在树中插入元素，循环操作，未完成自动更新深度功能
    void insert(const Type& value) {

        if (root == NULL) {
            root = new TreeNode<Type>(value);
            ++num_elements;
            return;
        }

        TreeNode<Type>* node = root;

        bool succeeded = false;
        while (!succeeded) {
            ++(node->subtree_scale);
            if (smaller_than(value, node->value)) {
                // insert to left
                if (node->left == NULL) {
                    // insert here
                    node->left = new TreeNode<Type>(value);
                    ++num_elements;
                    succeeded = true;
                } else {
                    // loop
                    node = node->left;
                }
            } else {
                // insert to right
                if (node->right == NULL) {
                    node->right = new TreeNode<Type>(value);
                    ++num_elements;
                    succeeded = true;
                } else {
                    // loop
                    node = node->right;
                }
            }
        }
    }

    // 从一个数组中插入元素
    void insertFromArray(Type input[], size_t num) {
        for (size_t index = 0; index < num; ++index) {
            insert(input[index]);
        }
    }

    // 前序遍历 node 及其子树
    void _PreOrder(TreeNode<Type>* node, node_func_t func, void_t func_for_null = EmptyFunc) {
        if (node == NULL) {
            func_for_null();
            return;
        }
        func(*node);
        _PreOrder(node->left, func, func_for_null);
        _PreOrder(node->right, func, func_for_null);
    }

    // 前序遍历整棵树
    void PreOrder(node_func_t func, void_t func_for_null = EmptyFunc) {
        _PreOrder(root, func, func_for_null);
    }

    // 中序遍历 node 及其子树
    void _InOrder(TreeNode<Type>* node, node_func_t func, void_t func_for_null = EmptyFunc) {
        if (node == NULL) {
            func_for_null();
            return;
        }
        _InOrder(node->left, func, func_for_null);
        func(*node);
        _InOrder(node->right, func, func_for_null);
    }

    // 中序遍历整棵树
    void InOrder(node_func_t func, void_t func_for_null = EmptyFunc) {
        _InOrder(root, func, func_for_null);
    }

    // 创建描述这棵树的形状的串
    void CreateShapeStr(char* str) {
        char* cur = _CreateShapeStr(str, str, root);
        *cur = '\0';
    }

    // 创建描述根为 node 的子树的形状的串
    char* _CreateShapeStr(char* beg, char* cur, TreeNode<Type>* node) {
        if (node == NULL) {
            return cur;
        }
        std::sprintf(cur, "#{");
        cur += 2;
        cur = _CreateShapeStr(beg, cur, node->left);
        std::sprintf(cur, ",");
        ++cur;
        cur = _CreateShapeStr(beg, cur, node->right);
        std::sprintf(cur, "}");
        ++cur;
        return cur;
    }

    // 获取整棵树的深度
    size_t depth() { return _depth(root); }

    // 获取根为 node 的子树的深度
    size_t _depth(TreeNode<Type>* node) {
        if (node == NULL) {
            return 0;
        }
        size_t left_depth = _depth(node->left);
        size_t right_depth = _depth(node->right);
        return (left_depth > right_depth ? left_depth + 1 : right_depth + 1);
    }

    // 形象地绘制树的形状，输出的是子树的深度，其中叶子的深度为 1
    void Illustrate(size_t indentation, TreeNode<Type>* node, node_func_t func) {
        for (size_t i = 0; i < indentation; ++i) {
            std::printf("    ");
        }

        if (node == NULL) {
            std::printf("NIL");
            return;
        }

        //std::printf("%d", node->depth);
        func(*node);

        if (node->left == NULL && node->right == NULL) {
            return;
        }

        std::printf(" --- {\n");
        Illustrate(indentation + 1, node->left, func);
        std::printf(",\n");
        Illustrate(indentation + 1, node->right, func);
        std::printf("\n");

        for (size_t i = 0; i < indentation; ++i) {
            std::printf("    ");
        }
        std::printf("}");
    }

    // A
    // +--- B
    // |    +--- D
    // |    +--- E
    // +----C
    // becomes
    // B
    // +--- D
    // +--- A
    //      +--- E
    //      +--- C
    void LLRotate(TreeNode<Type>*& node) {
        TreeNode<Type>* A = node;
        TreeNode<Type>* B = node->left;
        A->left = B->right;
        B->right = A;
        node = B;

        A->depth = (GetDepth(A->left) > GetDepth(A->right) ? GetDepth(A->left) : GetDepth(A->right)) + 1;
        B->depth = (GetDepth(B->left) > GetDepth(B->right) ? GetDepth(B->left) : GetDepth(B->right)) + 1;
        //Illustrate(0, *node);
    }

    // A
    // +--- B
    // +--- C
    //      +--- D
    //      +--- E
    // becomes
    // C
    // +--- A
    // |    +--- B
    // |    +--- D
    // +----E
    void RRRotate(TreeNode<Type>*& node) {
        TreeNode<Type>* A = node;
        TreeNode<Type>* C = node->right;
        A->right = C->left;
        C->left = A;
        node = C;
        
        A->depth = (GetDepth(A->left) > GetDepth(A->right) ? GetDepth(A->left) : GetDepth(A->right)) + 1;
        C->depth = (GetDepth(C->left) > GetDepth(C->right) ? GetDepth(C->left) : GetDepth(C->right)) + 1;
    }

    void LRRotate(TreeNode<Type>*& node) {
        RRRotate(node->left);
        LLRotate(node);
    }

    void RLRotate(TreeNode<Type>*& node) {
        LLRotate(node->right);
        RRRotate(node);
    }

    compare_t smaller_than;
    TreeNode<Type>* root;
    size_t num_elements;
};

template <class Type>
std::ostream& operator<<(std::ostream& os, const Tree<Type>& tree) {
    os << (tree.root) << std::endl;
    return os;
}

template <class Type>
std::ostream& operator<<(std::ostream& os, const TreeNode<Type>* node) {
    if (node == NULL) {
        os << ' ';
    } else {
        os << node->value;
        os << '<' << node->depth << "> [";
        os << node->left;
        os << ", " << node->right << ']';
    }
    return os;
}

#endif
