﻿#include <cstdio>
#include <cstring>
#include <cmath>
#include <iostream>
#include "tree_oldcpp.h"

struct Student {
    char number[6];
    float math;
    float chinese;
    float total;
};

bool operator==(const Student& lhs, const Student& rhs) {
    return (std::strcmp(lhs.number, rhs.number) == 0 && std::abs(lhs.math - rhs.math) < 1e-3
            && std::abs(lhs.chinese - rhs.chinese) < 1e-3);
}

// 测试用
std::ostream& operator<<(std::ostream& os, const Student& student);

// 返回 lhs.number < rhs.number
bool NumCompare(const Student& lhs, const Student& rhs);

// 返回 lhs.total > rhs.total，若 total 相等，则返回 lhs.number < rhs.number
bool ScoreCompare(const Student& lhs, const Student& rhs);

// 输出至 files::output
void PrintNodeToFile(const TreeNode<Student>& node);

// 测试用，输出至 stdout
void PrintNodeToStdout(const TreeNode<Student>& node);

// 测试用，输出至 fp
void PrintNode(std::FILE* fp, const TreeNode<Student>& node);

// 若 total >= 170 则输出
void PrintSearchedNodeToFile(const TreeNode<Student>& node);

// 测试用
void PrintNodeWithScore(const TreeNode<Student>& node);

namespace files {
std::FILE* input;
std::FILE* output;
}

int main(int argc, char* argv[]) {
    if (argc != 9) {
        return -1;
    }

    const char* input_ori_file_name = argv[1];
    const char* output_num_file_name = argv[2];
    const char* output_sco_file_name = argv[3];
    const char* output_search_file_name = argv[4];
    const char* input_new_file_name = argv[5];
    const char* output_update_file_name = argv[6];
    const char* input_del_file_name = argv[7];
    const char* output_del_file_name = argv[8];

    Tree<Student> numtree(NumCompare);

    Tree<Student> scoretree(ScoreCompare);

    // 1. 下面读入 input_ori
    if ((files::input = std::fopen(input_ori_file_name, "r")) == NULL) {
        return -1;
    }

    Student tempstu;
    while (std::fscanf(
                files::input,
                "%s%f%f",
                &tempstu.number,
                &tempstu.math,
                &tempstu.chinese
            ) == 3) {
        tempstu.total = tempstu.math + tempstu.chinese;
        numtree.AVLInsert(tempstu, numtree.root);
        scoretree.AVLInsert(tempstu, scoretree.root);
    }

    std::fclose(files::input);
    // 读入 input_ori 完成

    // 2. 下面输出 output_num
    if ((files::output = std::fopen(output_num_file_name, "w")) == NULL) {
        return -1;
    }

    numtree.InOrder(PrintNodeToFile);

    std::fclose(files::output);
    // 输出 output_num 完成

    // 3. 下面输出 output_sco
    if ((files::output = std::fopen(output_sco_file_name, "w")) == NULL) {
        return -1;
    }

    scoretree.InOrder(PrintNodeToFile);

    std::fclose(files::output);
    // 输出 output_sco 完成

    // 4. 下面输出 output_search
    if ((files::output = std::fopen(output_search_file_name, "w")) == NULL) {
        return -1;
    }

    scoretree.InOrder(PrintSearchedNodeToFile);

    std::fclose(files::output);
    // 输出 output_search 完成

    // 5. 下面进行增量更新，更新至 numtree
    if ((files::input = std::fopen(input_new_file_name, "r")) == NULL) {
        return -1;
    }

    while (std::fscanf(
                files::input,
                "%s%f%f",
                &tempstu.number,
                &tempstu.math,
                &tempstu.chinese
            ) == 3) {
        tempstu.total = tempstu.math + tempstu.chinese;
        numtree.AVLInsert(tempstu, numtree.root);
    }

    std::fclose(files::input);
    // 增量更新完成

    // 6. 下面输出 output_update
    if ((files::output = std::fopen(output_update_file_name, "w")) == NULL) {
        return -1;
    }

    numtree.InOrder(PrintNodeToFile);

    std::fclose(files::output);
    // 输出 output_update 完成

    // 7. 下面进行删除
    if ((files::input = std::fopen(input_del_file_name, "r")) == NULL) {
        return -1;
    }

    while (std::fscanf(
                files::input,
                "%s%f%f",
                &tempstu.number,
                &tempstu.math,
                &tempstu.chinese
            ) == 3) {
        tempstu.total = tempstu.math + tempstu.chinese;
        scoretree.AVLDelete(tempstu, scoretree.root);
    }

    std::fclose(files::input);
    // 删除完成

    // 8. 下面输出 output_del
    if ((files::output = std::fopen(output_del_file_name, "w")) == NULL) {
        return -1;
    }

    scoretree.InOrder(PrintNodeToFile);

    std::fclose(files::output);
    // 输出 output_del 完成

    // scoretree.Illustrate(0, scoretree.root, PrintNodeToStdout);

    return 0;

}

std::ostream& operator<<(std::ostream& os, const Student& student) {
    os << '('
       << student.number
       << ", "
       << student.math
       << ", "
       << student.chinese
       << ')'
       << std::endl;
    return os;
}

bool NumCompare(const Student& lhs, const Student& rhs) {
    return (std::strcmp(lhs.number, rhs.number) < 0);
}

bool ScoreCompare(const Student& lhs, const Student& rhs) {
    bool ret;
    if (lhs.total < rhs.total) {
        ret = false;
    } else if (lhs.total > rhs.total) {
        ret = true;
    } else {
        ret = NumCompare(lhs, rhs);
    }
    return ret;
}

void PrintNodeToFile(const TreeNode<Student>& node) {
    fprintf(files::output, "%s %.2f %.2f\n", node.value.number, node.value.math, node.value.chinese);
}

void PrintNodeToStdout(const TreeNode<Student>& node) {
    fprintf(stdout, "%s", node.value.number);
}

void PrintNode(std::FILE* fp, const TreeNode<Student>& node) {
    std::fprintf(fp, "%s %.2f %.2f\n", node.value.number, node.value.math, node.value.chinese);
}

void PrintSearchedNodeToFile(const TreeNode<Student>& node) {
    if (node.value.total >= 170
            && std::strcmp(node.value.number, "S3000") <= 0
            && std::strcmp(node.value.number, "S0001") >= 0) {
        std::fprintf(files::output, "%s %.2f %.2f\n", node.value.number, node.value.math, node.value.chinese);
    }
}

void PrintNodeWithScore(const TreeNode<Student>& node) {
    std::cout << '('
              << node.value.number
              << ", "
              << node.value.math
              << ", "
              << node.value.chinese
              << ", "
              << node.value.total
              << ')'
              << std::endl;
}
