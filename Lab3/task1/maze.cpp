// Caution:
// The program was originally written in C.
// However, in order to use lambda expression in C++11,
// I turn the program into C++.
// So you can see a lot of C-Style code. Sorry for that.

#include <cstdio>
#include <cstdlib>
#include <cstring>

#define NOT_TOUCHED '0'
#define WALL '1'
#define TOUCHED '2'
#define INTERSECTION '3'
#define SUCCESS '4'

// I wrote some checking code.
// If you want to enable check mod
// then uncomment the following definition
// The program will automatically print a new line of check

// #define CHECK

// If you want to go from exit to entrance
// then uncomment the following definition

//#define REVERSE

size_t height, width;
size_t entrance_row, entrance_col;
size_t exit_row, exit_col;
char** maze;

#ifdef CHECK
char** maze_backup;
int correct_ans = 1;
size_t before_row, before_col;
size_t after_row, after_col;
#endif

void printMaze();

void freeMaze();

int search(size_t start_row, size_t start_col);

int main() {
#ifdef REVERSE
    std::scanf(
        "%u%u%u%u%u%u",
        &height, &width,
        &entrance_row, &entrance_col,
        &exit_row, &exit_col
    );
#else
    std::scanf(
        "%u%u%u%u%u%u",
        &height, &width,
        &exit_row, &exit_col,
        &entrance_row, &entrance_col
    );
#endif

#ifdef CHECK
    after_row = exit_row + 1;
    after_col = exit_col;
#endif

    // Create maze: start
    maze = (char**)std::malloc(sizeof(char*) * height);
    for (size_t row = 0; row < height; ++row) {
        maze[row] = (char*)std::malloc(sizeof(char) * (width + 1));
        std::scanf("%s", maze[row]);
    }
    // Create maze: end

#ifdef CHECK
    // Create maze_backup: start
    maze_backup = (char**)std::malloc(sizeof(char*) * height);
    for (size_t row = 0; row < height; ++row) {
        maze_backup[row] = (char*)std::malloc(sizeof(char) * (width + 1));
        std::memcpy(maze_backup[row], maze[row], sizeof(char) * (width + 1));
    }
    // Create maze_backup: start
#endif
    search(entrance_row, entrance_col);
    freeMaze();

#ifdef CHECK
    std::printf("correct? %d\n", correct_ans);
#endif

    return 0;
}

void printMaze() {
    for (size_t row = 0; row < height; ++row) {
        for (size_t col = 0; col < width; ++col) {
            std::putchar(maze[row][col]);
        }
        std::putchar('\n');
    }
    std::putchar('\n');
}

void freeMaze() {
    for (size_t row = 0; row < height; ++row) {
        free(maze[row]);
        maze[row] = NULL;
    }
    free(maze);
    maze = NULL;

#ifdef CHECK
    for (size_t row = 0; row < height; ++row) {
        free(maze_backup[row]);
        maze_backup[row] = NULL;
    }
    free(maze_backup);
    maze_backup = NULL;
#endif

}

int search(size_t start_row, size_t start_col) {

    size_t row = start_row;
    size_t col = start_col;

    auto GoBackAndPrint = [&]() {
        while (row != start_row || col != start_col) {
            maze[row][col] = SUCCESS;
            std::printf("%u %u\n", row, col);
#if defined CHECK
            before_row = after_row;
            before_col = after_col;
            after_row = row;
            after_col = col;
            if ((before_col - after_col) * (before_col - after_col)
                    + (before_row - after_row) * (before_row - after_row) != 1) {
                correct_ans = 0;
            }
            if (maze_backup[row][col] != NOT_TOUCHED) {
                correct_ans = 0;
            }

#endif
            if (col < width - 1 && maze[row][col + 1] == TOUCHED) {
                ++col;
            } else if (row < height - 1 && maze[row + 1][col] == TOUCHED) {
                ++row;
            } else if (col > 0 && maze[row][col - 1] == TOUCHED) {
                --col;
            } else if (row > 0 && maze[row - 1][col] == TOUCHED) {
                --row;
            }

        }
        maze[row][col] = SUCCESS;
        std::printf("%u %u\n", row, col);
#if defined CHECK
        before_row = after_row;
        before_col = after_col;
        after_row = row;
        after_col = col;
        if ((before_col - after_col) * (before_col - after_col)
                + (before_row - after_row) * (before_row - after_row) != 1) {
            correct_ans = 0;
        }
        if (maze_backup[row][col] != NOT_TOUCHED) {
            correct_ans = 0;
        }
#endif
    };

    while (1) {
        int right = 0;
        int down = 0;
        int left = 0;
        int up = 0;

        // Ah! We have landed on a new tile
        // but haven't left our footprints!

        // First let's find out our situation:
        // Is it the exit?!
        if (row == exit_row && col == exit_col) {
            //maze[row][col] = TOUCHED;
            GoBackAndPrint();
            return 1;
        }

        // Search for the directions we can go.

        if (col < width - 1 && maze[row][col + 1] == NOT_TOUCHED) {
            right = 1;
        }
        if (row < height - 1 && maze[row + 1][col] == NOT_TOUCHED) {
            down = 1;
        }
        if (col > 0 && maze[row][col - 1] == NOT_TOUCHED) {
            left = 1;
        }
        if (row > 0 && maze[row - 1][col] == NOT_TOUCHED) {
            up = 1;
        }


        if (right + down + left + up == 1) {
            // If we has exactly one way to go, then go!

            maze[row][col] = TOUCHED;

            if (right == 1) {
                ++col;
            } else if (down == 1) {
                ++row;
            } else if (left == 1) {
                --col;
            } else {
                --row;
            }

        } else if (right + down + left + up > 1) {
            int ret = 0;
            // If we have multiple directions available,
            // then it is an intercection.

            maze[row][col] = INTERSECTION;
            if (right == 1) {
                ret = search(row, col + 1);
            }
            if (ret == 1) {
                //maze[row][col] = TOUCHED;
                GoBackAndPrint();
                return 1;
            }

            if (down == 1) {
                ret = search(row + 1, col);
            }
            if (ret == 1) {
                //maze[row][col] = TOUCHED;
                GoBackAndPrint();
                return 1;
            }

            if (left == 1) {
                ret = search(row, col - 1);
            }
            if (ret == 1) {
                //maze[row][col] = TOUCHED;
                GoBackAndPrint();
                return 1;
            }

            if (up == 1) {
                ret = search(row - 1, col);
            }
            if (ret == 1) {
                //maze[row][col] = TOUCHED;
                GoBackAndPrint();
                return 1;
            }

        } else if (right + down + left + up == 0) {
            // Too bad! We don't have any way to go.
            // Just get back.
            while (row != start_row || col != start_col) {

                maze[row][col] = WALL;

                if (col < width - 1 && maze[row][col + 1] == TOUCHED) {
                    ++col;
                } else if (row < height - 1 && maze[row + 1][col] == TOUCHED) {
                    ++row;
                } else if (col > 0 && maze[row][col - 1] == TOUCHED) {
                    --col;
                } else if (row > 0 && maze[row - 1][col] == TOUCHED) {
                    --row;
                }

            }
            maze[row][col] = WALL;
            return 0;
        }
    }
}
