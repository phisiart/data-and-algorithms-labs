// My own stack class
// Johnson Tan
// Dept. EE, Tsinghua University

#ifndef STACK_H
#define STACK_H

#include <iostream>
#include <stdexcept>

template <class Type> class Node;
template <class Type> class Stack;

// ostream output
// Style:
// [top] <top value> -> ... -> <bottom value> [bottom]
template <class Type>
std::ostream& operator<<(std::ostream& os, const Stack<Type>& stack);

// We use chain table inside the stack
// We don't want users to use this class
// so all members are private.
template <class Type>
class Node {
    friend class Stack<Type>;
    friend std::ostream& operator<< <Type>(
        std::ostream& os,
        const Stack<Type>& stack
    );
private:
    Node() : next(nullptr) {}
    Node(const Type& value) : value(value), next(nullptr) {}
    Type value;
    Node* next;
};

template <class Type>
class Stack {
    friend std::ostream& operator<< <Type>(
        std::ostream& os,
        const Stack<Type>& stack
    );
public:
    Stack() : top(nullptr) {}
    ~Stack() { clear(); }

    class iterator {
    public:
        iterator(Node<Type>* node = nullptr) : node(node) {}

        iterator(const iterator& iter) : node(iter.node) {}

        bool operator==(const iterator& rhs) {
            return node == rhs.node;
        }

        bool operator!=(const iterator& rhs) {
            return node != rhs.node;
        }


        Type& operator*() {
            if (node != nullptr) {
                return node->value;
            } else {
                throw std::out_of_range("Cannot access! Maybe iterator == end()?");
            }
        }
        Type* operator->() {
            return &(node->value);
        }

        iterator operator+(size_t addition) {
            Node<Type>* ret_node = node;
            for (size_t index = 0; index < addition; ++index) {
                if (ret_node == nullptr) {
                    return iterator(nullptr);
                }

                ret_node = ret_node->next;
            }

            return iterator(ret_node);
        }

        // ++iter
        iterator& operator++() {
            if (node == nullptr) {
                throw std::out_of_range("Error! iterator == end()?");
            }
            node = node->next;
            return *this;
        }

        // iter++
        iterator& operator++(int) {
            iterator ret(*this);
            if (node == nullptr) {
                throw std::out_of_range("Error! iterator == end()?");
            }
            node = node->next;
            return *this;
        }


    private:
        Node<Type>* node;

    }; // class iterator

    iterator begin() {
        return iterator(top);
    }

    iterator end() {
        return iterator(nullptr);
    }

    void push(const Type& value) {
        Node<Type>* new_node = new Node<Type>(value);
        new_node->next = top;
        top = new_node;
    }
    
    Type pop() {
        Node<Type>* top_temp = top;
        Type ret = top->value;
        top = top->next;
        delete top_temp;
        return ret;
    }

    Type top_val() const {
        return top->value;
    }

    void void_pop() {
        Node<Type>* top_temp = top;
        top = top->next;
        delete top_temp;
    }

    bool empty() const {
        return (top == nullptr);
    }

    void clear() {
        while (top != nullptr) {
            Node<Type>* top_temp = top;
            top = top->next;
            delete top_temp;
        }
    }

private:
    Node<Type>* top;
};

template <class Type>
std::ostream& operator<<(std::ostream& os, const Stack<Type>& stack) {
    if (stack.empty()) {
        return os;
    }
    os << "[top] " << stack.top->value;
    Node<Type>* node = stack.top->next;
    while (node != nullptr) {
        os << " -> " << node->value;
        node = node->next;
    }
    os << " [bottom]";
    return os;
}

#endif