#ifndef _MAZE_H
#define _MAZE_H

#include <cstdio>
#include "stack.h"

struct Record {
    Record(size_t row, size_t col, char type) :
        row(row), col(col), type(type) {}
    size_t row;
    size_t col;
    char type;
};

std::ostream& operator<<(std::ostream& os, const Record& record);

class Maze {
public:
    Maze() : maze(nullptr), maze_backup(nullptr) {}

    ~Maze() {
        free();
    }

    void load();
    void print();
    void printOrigin();
    void search();
    void printRecords();
    void printAnswers();
    bool checkAnswer();

private:
    void free();

    size_t height, width;
    size_t entrance_row, entrance_col;
    size_t exit_row, exit_col;
    char** maze;
    char** maze_backup;
    Stack<Record> records;

    static const bool reverse = false;
    static const bool check = false;
    static const char not_touched = '0';
    static const char wall = '1';
    static const char touched = '2';
    static const char intersection = '3';
};

#endif
