#include <cstring>
#include "maze.h"

std::ostream& operator<<(std::ostream& os, const Record& record) {
    os << '<';
    os << record.row;
    os << ", ";
    os << record.col;
    os << ">";
    return os;
}

void Maze::load() {
    free();

    if (reverse) {
        std::scanf(
            "%u%u%u%u%u%u",
            &height, &width,
            &entrance_row, &entrance_col,
            &exit_row, &exit_col
        );
    } else {
        std::scanf(
            "%u%u%u%u%u%u",
            &height, &width,
            &exit_row, &exit_col,
            &entrance_row, &entrance_col
        );
    }

    maze = new char*[height];
    for (size_t row = 0; row < height; ++row) {
        maze[row] = new char[width + 1];
        std::scanf("%s", maze[row]);
    }

    if (check) {
        maze_backup = new char*[height];
        for (size_t row = 0; row < height; ++row) {
            maze_backup[row] = new char[width + 1];
            std::memcpy(maze_backup[row], maze[row], sizeof(char) * (width + 1));
        }
    }

}

void Maze::print() {
    if (maze == nullptr) {
        return;
    }

    for (size_t row = 0; row < height; ++row) {
        for (size_t col = 0; col < width; ++col) {
            std::putchar(maze[row][col]);
        }
        std::putchar('\n');
    }
    std::putchar('\n');
}

void Maze::printOrigin() {
    if (maze_backup == nullptr) {
        return;
    }

    for (size_t row = 0; row < height; ++row) {
        for (size_t col = 0; col < width; ++col) {
            std::putchar(maze_backup[row][col]);
        }
        std::putchar('\n');
    }
    std::putchar('\n');
}

void Maze::free() {
    if (maze != nullptr) {
        for (size_t row = 0; row < height; ++row) {
            delete[] maze[row];
            maze[row] = nullptr;
        }
        delete[] maze;
        maze = nullptr;
    }

    if (maze_backup != nullptr) {
        for (size_t row = 0; row < height; ++row) {
            delete[] maze_backup[row];
            maze_backup[row] = nullptr;
        }
        delete[] maze_backup;
        maze_backup = nullptr;
    }
}

void Maze::printRecords() {
    std::cout << records << std::endl;
}

void Maze::printAnswers() {
    for (auto iter = records.begin(); iter != records.end(); ++iter) {
        std::printf("%u %u\n", iter->row, iter->col);
    }
}

bool Maze::checkAnswer() {
    bool ret = true;

    // 暂未考虑栈中只有一个元素，太烦了，不想搞
    Stack<Record>::iterator iter_1 = records.begin();
    auto iter_2 = iter_1 + 1;

    while (iter_2 != records.end()) {
        if ((iter_1->col - iter_2->col) * (iter_1->col - iter_2->col)
                + (iter_1->row - iter_2->row) * (iter_1->row - iter_2->row)
                != 1) {
            ret = false;
        }
        if (maze_backup[iter_1->row][iter_1->col] != not_touched) {
            ret = false;
        }

        ++iter_1;
        ++iter_2;
    }

    return ret;
}

void Maze::search() {
    size_t row = entrance_row;
    size_t col = entrance_col;

    while (1) {
        int right = 0;
        int down = 0;
        int left = 0;
        int up = 0;

        // Ah! We have landed on a new tile
        // but haven't left our footprints!

        // First let's find out our situation:
        // Is it the exit?!
        if (row == exit_row && col == exit_col) {
            maze[row][col] = touched;
            records.push(Record(row, col, touched));
            return;
        }

        // Search for the directions we can go.
        if (col < width - 1 && maze[row][col + 1] == not_touched) {
            right = 1;
        }
        if (row < height - 1 && maze[row + 1][col] == not_touched) {
            down = 1;
        }
        if (col > 0 && maze[row][col - 1] == not_touched) {
            left = 1;
        }
        if (row > 0 && maze[row - 1][col] == not_touched) {
            up = 1;
        }

        if (right + down + left + up == 1) {
            // If we has exactly one way to go, then go!

            maze[row][col] = touched;
            records.push(Record(row, col, touched));
            if (right == 1) {
                ++col;
            } else if (down == 1) {
                ++row;
            } else if (left == 1) {
                --col;
            } else {
                --row;
            }

        } else if (right + down + left + up > 1) {
            // If we have multiple directions available,
            // then it is an intercection.

            maze[row][col] = intersection;
            records.push(Record(row, col, intersection));

            if (right == 1) {
                ++col;
            } else if (down == 1) {
                ++row;
            } else if (left == 1) {
                --col;
            } else {
                --row;
            }

        } else if (right + down + left + up == 0) {
            // Too bad! We don't have any way to go.
            // Just get back.

            while (maze[row][col] != intersection && !records.empty()) {
                maze[row][col] = wall;
                Record record = records.pop();
                row = record.row;
                col = record.col;
            }

            if (maze[row][col] != intersection && records.empty()) {
                // Oops! Dead maze!
                return;
            }

            maze[row][col] = not_touched;
            // Notice we even erase the record of the intersection.

        }

    }

}
