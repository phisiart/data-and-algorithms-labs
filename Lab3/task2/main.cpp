#include <iostream>
#include "maze.h"

int main() {
    Maze maze;
    maze.load();
    maze.search();
    maze.printAnswers();
    return 0;
}
