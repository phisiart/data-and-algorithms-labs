﻿#include <stdio.h>
#include <stdlib.h>

#define ZERO 0
#define ADD_VALUE(x, y) ((x) + (y))
#define MULTIPLY_VALUE(x, y) ((x) * (y))
#define SINGLE_VALUE_PRINT "%d\t"
#define DUMMY_PRINT_FORMAT "%u %u %d\n"
#define SCAN_FORMAT "%u%u%d"

typedef int value_t;

typedef struct MatrixElement {
    size_t row;
    size_t col;
    struct MatrixElement* right;
    struct MatrixElement* down;
    value_t value;
} MatrixElement;

typedef struct MatrixHeader {
    size_t row;
    size_t col;
    struct MatrixElement* right;
    struct MatrixElement* down;
    struct MatrixHeader* next_header;
} MatrixHeader;

MatrixHeader* createHeaders(size_t height, size_t width);

MatrixElement* setElement(size_t row, size_t col, value_t value, MatrixHeader* header_base);

MatrixElement* getElement(size_t row, size_t col, MatrixHeader* header_base);

value_t getElementValue(size_t row, size_t col, MatrixHeader* header_base);

void clearMatrix(MatrixHeader* header_base);

void printMatrix(MatrixHeader* header_base);

void dummyPrintMatrix(MatrixHeader* header_base);

MatrixHeader* transposeMatrix(MatrixHeader* source);

MatrixHeader* addMatrix(MatrixHeader* lhs, MatrixHeader* rhs);

MatrixHeader* multiplyMatrix(MatrixHeader* lhs, MatrixHeader* rhs);

int main(void)
{
    MatrixHeader* A;
    MatrixHeader* B;
    MatrixHeader* C;
    MatrixHeader* D;

    size_t height, width, num_elements;
    size_t row, col;
    value_t value;
    size_t index;

    scanf("%u%u%d", &height, &width, &num_elements);
    A = createHeaders(height, width);

    for (index = 0; index < num_elements; ++index) {
        scanf(SCAN_FORMAT, &row, &col, &value);
        
        // The row / col stored in the matrix is 1 ~ height / width
        // However, the input is 0 ~ height - 1 / width - 1
        ++row;
        ++col;

        setElement(row, col, value, A);
    }

    B = transposeMatrix(A);
    C = addMatrix(A, B);
    D = multiplyMatrix(A, B);

    //dummyPrintMatrix(A);
    dummyPrintMatrix(B);
    dummyPrintMatrix(C);
    dummyPrintMatrix(D);

    /*printMatrix(A);
    printMatrix(B);
    printMatrix(C);
    printMatrix(D);*/

    clearMatrix(A);
    clearMatrix(B);
    clearMatrix(C);
    clearMatrix(D);
    return 0;
}

MatrixHeader* createHeaders(size_t height, size_t width) {
    MatrixHeader* header_base;      // The header of all headers
    MatrixHeader* header;
    size_t index;
    size_t size;

    header_base = (MatrixHeader*)malloc(sizeof(MatrixHeader));
    header_base->col = width;
    header_base->row = height;

    size = (width > height) ? width : height;
    header = header_base;
    for (index = 0; index < size; ++index) {
        // Create a new header next to the current header
        header->next_header = (MatrixHeader*)malloc(sizeof(MatrixHeader));
        header = header->next_header;
        header->col = 0;
        header->row = 0;
        header->right = (MatrixElement*)header;
        header->down = (MatrixElement*)header;
    }

    header->next_header = header_base;

    return header_base;
}

MatrixElement* setElement(size_t row, size_t col, value_t value, MatrixHeader* header_base) {
    // Element should be inserted between before and after.    
    MatrixElement* row_before;
    MatrixElement* col_before;
    MatrixElement* row_after;
    MatrixElement* col_after;
    MatrixHeader* row_header;
    MatrixHeader* col_header;

    MatrixElement* element;

    size_t index;

    if (header_base == NULL) {
        return NULL;
    }

    if ((row > header_base->row) || (col > header_base->col)) {
        return NULL;
    }

    //-----------------------------------------------------------
    row_header = header_base;

    for (index = 0; index < row; ++index) {
        row_header = row_header->next_header;
    }
    // Now row_header = row_header[row]

    row_before = (MatrixElement*)row_header;
    row_after = row_before->right;

    while (row_after != (MatrixElement*)row_header) {
        if (row_after->col >= col) {
            break;
        }
        row_before = row_after;
        row_after = row_after->right;
    }

    if (row_after->col == col) {
        // The element already exists

        if (value == ZERO) {
            
            // Delete element.

            col_header = header_base;

            for (index = 0; index < col; ++index) {
                col_header = col_header->next_header;
            }
            // Now col_header = header[col]

            col_before = (MatrixElement*)col_header;
            col_after = col_before->down;

            while (col_after != (MatrixElement*)col_header) {
                if (col_after->row >= row) {
                    break;
                }
                col_before = col_after;
                col_after = col_after->down;
            }

            row_before->right = row_after->right;
            col_before->down = col_after->down;
            
            free(row_after);
            //free(col_after);
            return NULL;
        }

        row_after->value = value;
        return row_after;
    }

    // The element needs to be created.

    if (value == ZERO) {
        return NULL;
    }

    element = (MatrixElement*)malloc(sizeof(MatrixElement));
    element->col = col;
    element->row = row;
    element->value = value;

    row_before->right = element;
    element->right = row_after;

    //-----------------------------------------------------------
    col_header = header_base;
    for (index = 0; index < col; ++index) {
        col_header = col_header->next_header;
    }
    // Now col_header = col_header[col]

    col_before = (MatrixElement*)col_header;
    col_after = col_before->down;

    while (col_after != (MatrixElement*)col_header) {
        if (col_after->row > row) {
            break;
        }
        col_before = col_after;
        col_after = col_after->down;
    }
    col_before->down = element;
    element->down = col_after;

    return element;
}

MatrixElement* getElement(size_t row, size_t col, MatrixHeader* header_base) {
    MatrixHeader* header = header_base;
    MatrixElement* element;
    size_t index_row;

    if (header_base == NULL) {
        return NULL;
    }

    for (index_row = 0; index_row < row; ++index_row) {
        header = header->next_header;
    }
    // header[row]

    element = header->right;
    while (element->col != 0 && element->col < col) {
        element = element->right;
    }

    if (element->col != col) {
        return NULL;
    }

    return element;
}

value_t getElementValue(size_t row, size_t col, MatrixHeader* header_base) {
    MatrixElement* element = getElement(row, col, header_base);
    if (element == NULL) {
        return 0;
    }
    return element->value;
}

void clearMatrix(MatrixHeader* header_base) {
    MatrixHeader* header;
    MatrixHeader* temp_header;
    MatrixElement* element;
    MatrixElement* temp_element;

    if (header_base == NULL) {
        return;
    }

    header = header_base->next_header;
    while (header != header_base) {
        element = header->right;
        while ((MatrixHeader*)element != header) {
            temp_element = element;
            element = element->right;
            free(temp_element);
        }

        temp_header = header;
        header = header->next_header;
        free(temp_header);
    }
    free(header_base);
    header_base = NULL;
}

void printMatrix(MatrixHeader* header_base) {
    MatrixHeader* header = header_base;
    MatrixElement* element;
    size_t width;
    size_t height;
    size_t row, col;

    if (header_base == NULL) {
        printf("ERROR MATRIX\n");
    }

    width = header_base->col;
    height = header_base->row;
    
    for (row = 1; row <= height; ++row) {
        header = header->next_header; // header[row]
        col = 1;
        
        for (element = header->right; element->col != 0; element = element->right) {
            while (col++ < element->col) {
                printf("0\t");
            }
            printf(SINGLE_VALUE_PRINT, element->value);
        }
        for (; col <= width; ++col) {
            printf("0\t");
        }

        putchar('\n');
    }

}

void dummyPrintMatrix(MatrixHeader* header_base) {
    MatrixHeader* header = header_base;
    MatrixElement* element;
    size_t height = header_base->row;
    size_t row;

    if (header_base == NULL) {
        printf("ERROR MATRIX\n");
    }

    for (row = 0; row < height; ++row) {
        header = header->next_header;
        element = header->right;
        while ((MatrixHeader*)element != header) {
            if (element->value != ZERO) {
                printf(DUMMY_PRINT_FORMAT, element->row - 1, element->col - 1, element->value);
            }
            element = element->right;
        }

    }

}

MatrixHeader* transposeMatrix(MatrixHeader* source) {
    MatrixHeader* header_base;
    MatrixHeader* header;
    size_t height = source->col;
    size_t width = source->row;
    MatrixHeader** col_headers;
    MatrixHeader** row_headers;
    MatrixElement** col_fronts;
    MatrixElement** row_fronts;
    MatrixElement* source_element;
    MatrixElement* element;
    size_t row, col;

    if (source == NULL) {
        return NULL;
    }

    header_base = createHeaders(height, width);

    col_headers = (MatrixHeader**)malloc(sizeof(MatrixHeader*) * width);
    col_fronts = (MatrixElement**)malloc(sizeof(MatrixElement*) * width);
    row_headers = (MatrixHeader**)malloc(sizeof(MatrixHeader*) * height);
    row_fronts = (MatrixElement**)malloc(sizeof(MatrixElement*) * height);

    header = header_base->next_header;
    for (col = 0; col < width; ++col) {
        col_headers[col] = header;
        col_fronts[col] = (MatrixElement*)header;
        header = header->next_header;
    }

    header = header_base->next_header;
    for (row = 0; row < height; ++row) {
        row_headers[row] = header;
        row_fronts[row] = (MatrixElement*)header;
        header = header->next_header;
    }

    for (row = 1; row <= height; ++row) {
        // Get to column[row] of source
        source = source->next_header;

        for (
            source_element = source->down;
            source_element != (MatrixElement*)source;
            source_element = source_element->down
        ) {
            element = (MatrixElement*)malloc(sizeof(MatrixElement));
            element->col = source_element->row;
            element->row = row;
            element->value = source_element->value;
            row_fronts[row - 1]->right = element;
            row_fronts[row - 1] = element;
            col_fronts[element->col - 1]->down = element;
            col_fronts[element->col - 1] = element;
        }

        
    }

    for (row = 0; row < height; ++row) {
        row_fronts[row]->right = (MatrixElement*)row_headers[row];
    }

    for (col = 0; col < width; ++col) {
        col_fronts[col]->down = (MatrixElement*)col_headers[col];
    }

    return header_base;
}

MatrixHeader* addMatrix(MatrixHeader* lhs, MatrixHeader* rhs) {
    MatrixHeader* lhs_header;
    MatrixHeader* rhs_header;
    MatrixElement* lhs_element;
    MatrixElement* rhs_element;
    MatrixElement* element;
    MatrixHeader* header_base;
    MatrixHeader* header;

    MatrixHeader** col_headers;
    MatrixHeader** row_headers;
    MatrixElement** col_fronts;
    MatrixElement** row_fronts;

    size_t width, height;
    size_t col, row;
    value_t value;

    if (lhs == NULL || rhs == NULL) {
        return NULL;
    }

    if (lhs->col != rhs->col || lhs->row != rhs->row) {
        return NULL;
    }

    width = lhs->col;
    height = lhs->row;

    header_base = createHeaders(height, width);

    col_headers = (MatrixHeader**)malloc(sizeof(MatrixHeader*) * width);
    col_fronts = (MatrixElement**)malloc(sizeof(MatrixElement*) * width);
    row_headers = (MatrixHeader**)malloc(sizeof(MatrixHeader*) * height);
    row_fronts = (MatrixElement**)malloc(sizeof(MatrixElement*) * height);

    header = header_base->next_header;
    for (col = 0; col < width; ++col) {
        col_headers[col] = header;
        col_fronts[col] = (MatrixElement*)header;
        header = header->next_header;
    }

    header = header_base->next_header;
    for (row = 0; row < height; ++row) {
        row_headers[row] = header;
        row_fronts[row] = (MatrixElement*)header;
        header = header->next_header;
    }

    lhs_header = lhs;
    rhs_header = rhs;
    for (row = 1; row <= height; ++row) {
        // Get to the header of this row
        lhs_header = lhs_header->next_header;
        rhs_header = rhs_header->next_header;

        lhs_element = lhs_header->right;
        rhs_element = rhs_header->right;
        while ((MatrixHeader*)lhs_element != lhs_header || (MatrixHeader*)rhs_element != rhs_header) {
            if ((lhs_element->col > rhs_element->col && rhs_element->col != 0) || (MatrixHeader*)lhs_element == lhs_header) {
                // Copy the element in rhs
                element = (MatrixElement*)malloc(sizeof(MatrixElement));
                element->row = row;
                element->col = rhs_element->col;
                element->value = rhs_element->value;
                row_fronts[row - 1]->right = element;
                row_fronts[row - 1] = element;
                col_fronts[col - 1]->down = element;
                col_fronts[col - 1] = element;

                rhs_element = rhs_element->right;

            } else if ((lhs_element->col < rhs_element->col && lhs_element->col != 0) || (MatrixHeader*)rhs_element == rhs_header) {
                // Copy the element in lhs
                element = (MatrixElement*)malloc(sizeof(MatrixElement));
                element->row = row;
                element->col = lhs_element->col;
                element->value = lhs_element->value;
                row_fronts[row - 1]->right = element;
                row_fronts[row - 1] = element;
                col_fronts[col - 1]->down = element;
                col_fronts[col - 1] = element;

                lhs_element = lhs_element->right;

            } else { // lhs_element->col == rhs_element->col

                // Only non-zero value would be added into the matrix

                if ((value = ADD_VALUE(lhs_element->value, rhs_element->value)) != ZERO) {
                    element = (MatrixElement*)malloc(sizeof(MatrixElement));
                    element->col = lhs_element->col;
                    element->row = row;
                    element->value = value;

                    row_fronts[row - 1]->right = element;
                    row_fronts[row - 1] = element;
                    col_fronts[col - 1]->down = element;
                    col_fronts[col - 1] = element;
                }
                lhs_element = lhs_element->right;
                rhs_element = rhs_element->right;

            }
        }

    }

    for (row = 0; row < height; ++row) {
        row_fronts[row]->right = (MatrixElement*)row_headers[row];
    }

    for (col = 0; col < width; ++col) {
        col_fronts[col]->down = (MatrixElement*)col_headers[col];
    }

    return header_base;
}

MatrixHeader* multiplyMatrix(MatrixHeader* lhs, MatrixHeader* rhs) {
    size_t col, row;
    size_t width, height;
    MatrixHeader* lhs_header;
    MatrixHeader* rhs_header;
    MatrixHeader* header_base;
    MatrixHeader* header;

    MatrixHeader** col_headers;
    MatrixHeader** row_headers;

    MatrixElement** col_fronts;
    MatrixElement** row_fronts;

    MatrixElement* lhs_element;
    MatrixElement* rhs_element;

    MatrixElement* element;

    value_t sum;

    if (lhs == NULL || rhs == NULL) {
        return NULL;
    }

    if (lhs->col != rhs->row) {
        return NULL;
    }

    height = lhs->row;
    width = rhs->col;

    header_base = createHeaders(height, width);

    col_headers = (MatrixHeader**)malloc(sizeof(MatrixHeader*) * width);
    col_fronts = (MatrixElement**)malloc(sizeof(MatrixElement*) * width);
    row_headers = (MatrixHeader**)malloc(sizeof(MatrixHeader*) * height);
    row_fronts = (MatrixElement**)malloc(sizeof(MatrixElement*) * height);

    header = header_base->next_header;
    for (col = 0; col < width; ++col) {
        col_headers[col] = header;
        col_fronts[col] = (MatrixElement*)header;
        header = header->next_header;
    }

    header = header_base->next_header;
    for (row = 0; row < height; ++row) {
        row_headers[row] = header;
        row_fronts[row] = (MatrixElement*)header;
        header = header->next_header;
    }

    lhs_header = lhs;
    for (row = 1; row <= height; ++row) {
        lhs_header = lhs_header->next_header;

        rhs_header = rhs;
        for (col = 1; col <= width; ++col) {

            rhs_header = rhs_header->next_header;

            // Pick a row from lhs and a col from rhs
            sum = ZERO;
            lhs_element = lhs_header->right;
            rhs_element = rhs_header->down;
            while ((MatrixHeader*)lhs_element != lhs_header && (MatrixHeader*)rhs_element != rhs_header) {
                if (lhs_element->col > rhs_element->row) {
                    rhs_element = rhs_element->down;

                } else if (lhs_element->col < rhs_element->row) {
                    lhs_element = lhs_element->right;

                } else { // lhs_element->col == rhs_element->row

                    // sum += lhs_element->value * rhs_element->value
                    sum = ADD_VALUE(sum, MULTIPLY_VALUE(lhs_element->value, rhs_element->value));
                    
                    lhs_element = lhs_element->right;
                    rhs_element = rhs_element->down;

                }
            }

            // Only non-zero element would be added into the matrix
            if (sum != ZERO) {
                element = (MatrixElement*)malloc(sizeof(MatrixElement));
                element->col = col;
                element->row = row;
                element->value = sum;
                row_fronts[row - 1]->right = element;
                row_fronts[row - 1] = element;
                col_fronts[col - 1]->down = element;
                col_fronts[col - 1] = element;
            }

        } // end for (col = 1; col <= width; ++col)
    } // end for (row = 1; row <= height; ++row)

    for (row = 0; row < height; ++row) {
        row_fronts[row]->right = (MatrixElement*)row_headers[row];
    }

    for (col = 0; col < width; ++col) {
        col_fronts[col]->down = (MatrixElement*)col_headers[col];
    }

    return header_base;
}

