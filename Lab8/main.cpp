#include <cstdio>
#include <cmath>

void SolveProb1(double p1, double p2, double* x1, double* x2);

class Mat3 {
public:
    Mat3(
        double a11, double a12, double a13,
        double a21, double a22, double a23,
        double a31, double a32, double a33
    ) : a11(a11), a12(a12), a13(a13),
        a21(a21), a22(a22), a23(a23),
        a31(a31), a32(a32), a33(a33) {}

    double Det() {
        return a11 * a22 * a33
               + a12 * a23 * a31
               + a13 * a21 * a32
               - a11 * a23 * a32
               - a12 * a21 * a33
               - a13 * a22 * a31;
    }

    double a11, a12, a13;
    double a21, a22, a23;
    double a31, a32, a33;

};

class Prob2 {
public:
    Prob2(double a, double b, double c, double d)
        : a(a), b(b), c(c), d(d) {}

    double f(double x1, double x2) {
        return std::exp(a * x1) - b * std::log(x1 + x2) + c * x1 * x1 + d * x2 * x2;
    }

    void gradf(double x1, double x2, double* ret1, double* ret2) {
        *ret1 = a * std::exp(a * x1) - b / (x1 + x2) + 2 * c * x1;
        *ret2 = -b / (x1 + x2) + 2 * d * x2;
    }

    double g(double x) {
        return a * a * std::exp(2 * a * x) + 2 * a * (2 * c + d) * x * std::exp(a * x) + 4 * c * (c + d) * x * x - 2 * b * d;
    }

    double gprime(double x) {
        return 2 * a * a * a * std::exp(2 * a * x) + 2 * a * (2 * c + d) * (1 + a * x) * std::exp(a * x) + 8 * c * (c + d) * x;
    }

    double GetX(double x0) {
        double x = x0;
        x0 = x - 1;
        while (std::abs(x - x0) > 1e-6) {
            x0 = x;
            x -= g(x) / gprime(x);
        }
        return x;
    }

    void solve(double x0, double ret[2], double* func) {
        double x = GetX(x0);
        ret[0] = x;
        double y = b / (a * std::exp(a * x) + 2 * c * x) - x;
        ret[1] = y;
        *func = f(x, y);
    }

    double a, b, c, d;
};

class Prob3 {
public:
    Prob3(
        double h1, double h2, double h3,
        double a11, double a12, double a13,
        double a21, double a22, double a23,
        double b1, double b2
    ) : h1(h1), h2(h2), h3(h3),
        a11(a11), a12(a12), a13(a13),
        a21(a21), a22(a22), a23(a23),
        b1(b1), b2(b2) {}

    void Solve(double ret[3], double* f) {
        double d1 = a12 * a23 - a13 * a22;
        double d2 = a13 * a21 - a11 * a23;
        double d3 = a11 * a22 - a12 * a21;

        double a31 = d1 * h1;
        double a32 = d2 * h2;
        double a33 = d3 * h3;

        double b3 = 0;

        double D = Mat3(a11, a12, a13, a21, a22, a23, a31, a32, a33).Det();
        double D1 = Mat3(b1, a12, a13, b2, a22, a23, b3, a32, a33).Det();
        double D2 = Mat3(a11, b1, a13, a21, b2, a23, a31, b3, a33).Det();
        double D3 = Mat3(a11, a12, b1, a21, a22, b2, a31, a32, b3).Det();

        ret[0] = D1 / D;
        ret[1] = D2 / D;
        ret[2] = D3 / D;

        *f = h1 * ret[0] * ret[0] + h2 * ret[1] * ret[1] + h3 * ret[2] * ret[2];

    }

    double h1, h2, h3;
    double a11, a12, a13;
    double a21, a22, a23;
    double b1, b2;

};

int main() {
    // prob1
    double p1, p2, p3, p4;
    double x1, x2, x3;
    std::scanf("%lf%lf", &x1, &x2);
    std::scanf("%lf%lf%lf%lf", &p1, &p2, &p3, &p4);
    SolveProb1(p1, p2, &x1, &x2);
    double f1 = -std::log(1 - p1 * x1 - p2 * x2) - std::log(p3 * x1) - std::log(p4 * x2);
    std::printf("%.4f %.4f %.4f\n", f1, x1, x2);

    // prob2
    double a, b, c, d;
    std::scanf("%lf%lf", &x1, &x2);
    std::scanf("%lf%lf%lf%lf", &a, &b, &c, &d);
    Prob2 prob2(a, b, c, d);
    double ret2[3];
    prob2.solve(x1, ret2, ret2 + 2);
    std::printf("%.4f %.4f %.4f\n", ret2[2], ret2[0], ret2[1]);

    // prob3
    double h1, h2, h3;
    /*double a11 = 2, a12 = 5, a13 = -2;
    double a21 = 1, a22 = -2, a23 = 5;
    double b1 = 9, b2 = -6;*/
    double a11, a12, a13;
    double a21, a22, a23;
    double b1, b2;
    std::scanf("%lf%lf%lf", &x1, &x2, &x3);
    std::scanf("%lf%lf%lf", &h1, &h2, &h3);
    std::scanf("%lf%lf%lf", &a11, &a12, &a13);
    std::scanf("%lf%lf%lf", &a21, &a22, &a23);
    std::scanf("%lf%lf", &b1, &b2);
    Prob3 prob3(h1, h2, h3, a11, a12, a13, a21, a22, a23, b1, b2);
    double ret[3];
    double f;
    prob3.Solve(ret, &f);
    std::printf("%.4f %.4f %.4f %.4f\n", f, ret[0], ret[1], ret[2]);

    return 0;
}

void SolveProb1(double p1, double p2, double* x1, double* x2) {
    *x1 = 1.0 / 3 / p1;
    *x2 = 1.0 / 3 / p2;
}
