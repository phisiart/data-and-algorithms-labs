#ifndef _TREE_H
#define _TREE_H

#include <iostream>
#include <cstdio>
#include <functional>

template <class Type> class TreeNode;

template <class Type> class Tree;

template <class Type>
std::ostream& operator<<(std::ostream& os, const Tree<Type>& tree);

template <class Type>
std::ostream& operator<<(std::ostream& os, const TreeNode<Type>& node);

template <class Type>
class TreeNode {
    friend std::ostream& operator<< <Type>(std::ostream& os, const TreeNode& node);
public:
    TreeNode() : subtree_scale(1), left(nullptr), right(nullptr) {}
    TreeNode(const Type& value) : subtree_scale(1), value(value), left(nullptr), right(nullptr) {}
    Type value;
    size_t subtree_scale;
    TreeNode* left;
    TreeNode* right;
};

template <class Type>
class Tree {
    friend std::ostream& operator<< <Type>(std::ostream& os, const Tree& tree);

public:

    typedef std::function<bool(const Type&, const Type&)> compare_t;

    Tree(const compare_t& less_than) : less_than(less_than), root(nullptr), num_elements(0) {}

    void _free(TreeNode<Type>* node) {
        if (node == nullptr) {
            return;
        }
        _free(node->left);
        _free(node->right);
        delete node;
        node = nullptr;
    }

    void free() {
        _free(root);
    }

    ~Tree() { free(); }

    // recursive version
    void _insert(const Type& value, TreeNode<Type>* node) {
        
        if (root == nullptr) {
            root = new TreeNode<Type>(value);
            ++num_elements;
            return;
        }
        ++(node->subtree_scale);
        if (less_than(value, node->value)) {
            // insert to left
            if (node->left == nullptr) {
                // insert here
                node->left = new TreeNode<Type>(value);
                ++num_elements;
            } else {
                // recursion
                _insert(value, node->left);
            }
        } else {
            // insert to right
            if (node->right == nullptr) {
                // insert here
                node->right = new TreeNode<Type>(value);
                ++num_elements;
            } else {
                // recursion
                _insert(value, node->right);
            }
        }
    }

    // non-recursive version
    void insert(const Type& value) {
        
        if (root == nullptr) {
            root = new TreeNode<Type>(value);
            ++num_elements;
            return;
        }
        TreeNode<Type>* node = root;
        bool succeeded = false;
        while (!succeeded) {
            ++(node->subtree_scale);
            if (less_than(value, node->value)) {
                // insert to left
                if (node->left == nullptr) {
                    // insert here
                    node->left = new TreeNode<Type>(value);
                    ++num_elements;
                    succeeded = true;
                } else {
                    // loop
                    node = node->left;
                }
            } else {
                // insert to right
                if (node->right == nullptr) {
                    node->right = new TreeNode<Type>(value);
                    ++num_elements;
                    succeeded = true;
                } else {
                    // loop
                    node = node->right;
                }
            }
        }
    }

    void insertFromArray(Type input[], size_t num) {
        for (size_t index = 0; index < num; ++index) {
            insert(input[index]);
        }
    }

    // pre-order traversal
    void _PreOrder(
        TreeNode<Type>* node,
        std::function<void(TreeNode<Type>&)> func,
        std::function<void()> func_for_null = [](){}
    ) {
        if (node == nullptr) {
            func_for_null();
            return;
        }
        func(*node);
        _PreOrder(node->left, func, func_for_null);
        _PreOrder(node->right, func, func_for_null);
    }

    void CreateShapeStr(char* str) {
        char* cur = _CreateShapeStr(str, str, root);
        *cur = '\0';
    }

    // return current
    char* _CreateShapeStr(char* beg, char* cur, TreeNode<Type>* node) {
        if (node == nullptr) {
            return cur;
        }
        std::sprintf(cur, "#{");
        cur += 2;
        cur = _CreateShapeStr(beg, cur, node->left);
        std::sprintf(cur, ",");
        ++cur;
        cur = _CreateShapeStr(beg, cur, node->right);
        std::sprintf(cur, "}");
        ++cur;
        return cur;
    }

    size_t depth() { return _depth(root); }

    size_t _depth(TreeNode<Type>* node) {
        if (node == nullptr) {
            return 0;
        }
        size_t left_depth = _depth(node->left);
        size_t right_depth = _depth(node->right);
        return (left_depth > right_depth ? left_depth + 1 : right_depth + 1);
    }

    std::function<bool(const Type&, const Type&)> less_than;
    TreeNode<Type>* root;
    size_t num_elements;
};

template <class Type>
std::ostream& operator<<(std::ostream& os, const Tree<Type>& tree) {
    os << (tree.root) << std::endl;
    return os;
}

template <class Type>
std::ostream& operator<<(std::ostream& os, const TreeNode<Type>* node) {
    if (node == nullptr) {
        os << ' ';
    } else {
        os << node->value;
        os << '<' << node->subtree_scale << "> [";
        os << node->left;
        os << ", " << node->right << ']';
    }
    return os;
}

#endif