﻿--- README ---

1. This program is written in standard C++11, and is tested with
    1) Microsoft Visual Studio 2013
    2) g++ -std=c++11 *.cpp
        Nootice that the flag "-std=c++11" cannot be ignored, since I have used "nullptr"s and lambda-expressions.

2. Lab4 contains **two** problems. I have written them in one program. The program could automatically determine which to run: 
    if argc == 3 (eg. main_1.exe input.txt output.txt) then run problem 1;
    if argc == 4 (eg. main_2.exe parent.txt child.txt output.txt) then rum problem 2.

    That's why you could see that the two files in the "exe" folder are **exactly** the same.
