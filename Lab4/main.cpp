﻿#include "tree.h"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>

const int debug = 0;

void CreateNexts(const char* source, size_t source_len, size_t next[]);
int FindFirstSubStr(const char* source, size_t source_len, const char* substr, size_t substr_len, size_t beg, size_t next[]);
size_t FindFirstSubstrs(const char* source, const char* substr, size_t ret[], size_t ret_size);
int _main_1(char* input, char* output);
int _main_2(char* input_parent, char* input_son, char* output);

int main(int argc, char* argv[]) {
    if (argc == 3) {
        return _main_1(argv[1], argv[2]);
    } else if (argc == 4) {
        return _main_2(argv[1], argv[2], argv[3]);
    }
}

void CreateNexts(const char* source, size_t source_len, size_t next[]) {
    std::memset(next, 0, sizeof(size_t)* source_len);

    for (size_t beg = source_len - 1; beg > 0; --beg) {
        for (size_t size = 0; (beg + size < source_len) && (source[size] == source[beg + size]); ++size) {
            next[beg + size] = size + 1;
        }
    }
}

int FindFirstSubStr(const char* source, size_t source_len, const char* substr, size_t substr_len, size_t beg, size_t next[]) {
    source += beg;
    // 0 1 2 3 4 5 6 ... pos
    // pos - next[pos - 1] -> 0
    while (source_len - beg >= substr_len) {
        size_t pos = 0;

        while (pos < substr_len) {
            if (source[pos] != substr[pos]) {
                break;
            }
            ++pos;
        }
        
        // 现在 pos 是不匹配位
        // 如果不匹配位置是 substr_len 即 substr 末尾出头的位置，说明匹配成功。

        if (pos == substr_len) {
            // succeeded
            return beg;
        } else {
            if (pos == 0) {
                ++beg;
                ++source;
            } else {
                beg += pos - next[pos - 1];
                source += pos - next[pos - 1];
            }
        }
    }
    return -1;
}

size_t FindFirstSubstrs(const char* source, const char* substr, size_t ret[], size_t ret_size) {
    size_t count = 0;
    size_t source_len = std::strlen(source);
    size_t substr_len = std::strlen(substr);
    size_t beg = 0;

    size_t* next = new size_t[std::strlen(substr)];
    CreateNexts(substr, std::strlen(substr), next);
    
    if (debug) {
        for (size_t i = 0; i < std::strlen(substr); ++i) {
            std::cout << next[i] << '\t';
        }
        std::cout << std::endl;
    }
    do {
        int pos_found = FindFirstSubStr(source, source_len, substr, substr_len, beg, next);
        if (pos_found == -1) {
            break;
        }
        ret[count] = pos_found;
        ++count;
        beg = pos_found + substr_len - next[substr_len - 1];
    } while (count < ret_size);

    delete[] next;

    return count;
}

int _main_1(char* input, char* output) {
    std::FILE* fin;
    if ((fin = std::fopen(input, "r")) == nullptr) {
        throw "error opening file.";
    }
    std::FILE* fout;
    if ((fout = std::fopen(output, "w")) == nullptr) {
        throw "error opening file.";
    }

    // You have to give the tree a comparing function.
    Tree<int> tree([](const int& lhs, const int& rhs) { return lhs < rhs; });

    int num;
    while (std::fscanf(fin, "%d", &num) == 1) {
        tree.insert(num);
    }

    tree._PreOrder(
        tree.root,
        [&](TreeNode<int>& node) { std::fprintf(fout, "%d ", node.value); },
        [&]() { std::fprintf(fout, "# "); }
    );

    std::fprintf(fout, "\n\n");

    tree._PreOrder(
        tree.root,
        [&](TreeNode<int>& node) { std::fprintf(fout, "%d ", node.subtree_scale); },
        [&]() { std::fprintf(fout, "# "); }
    );

    std::fprintf(fout, "\n\n");

    //std::ostringstream os;
    //os << tree;
    //std::fprintf(fout, "%s\n", os.str().c_str());
    
    std::fprintf(fout, "%u", tree.depth());

    std::fclose(fin);
    std::fclose(fout);

    return 0;
}

int _main_2(char* input_parent, char* input_son, char* output) {
    std::FILE* fin_parent;
    if ((fin_parent = std::fopen(input_parent, "r")) == nullptr) {
        throw "error opening file.";
    }
    std::FILE* fin_son;
    if ((fin_son = std::fopen(input_son, "r")) == nullptr) {
        throw "error opening file.";
    }
    std::FILE* fout;
    if ((fout = std::fopen(output, "w")) == nullptr) {
        throw "error opening file.";
    }

    // You have to give the tree a comparing function.
    Tree<int> parent([](const int& lhs, const int& rhs) { return lhs < rhs; });
    Tree<int> son([](const int& lhs, const int& rhs) { return lhs < rhs; });

    int num;
    while (std::fscanf(fin_parent, "%d", &num) == 1) {
        parent.insert(num);
    }
    while (std::fscanf(fin_son, "%d", &num) == 1) {
        son.insert(num);
    }

    char* parent_shape = new char[parent.num_elements * 4 + 1];
    char* son_shape = new char[son.num_elements * 4 + 1];

    parent.CreateShapeStr(parent_shape);
    son.CreateShapeStr(son_shape);
    
    if (debug) {
        std::fprintf(fout, "Parent shape:\n");
        std::fprintf(fout, "%s\n", parent_shape);
        size_t level = 1;
        for (size_t i = 0; i < std::strlen(parent_shape); ++i) {
            if (parent_shape[i] == '{') {
                ++level;
            } else if (parent_shape[i] == '}') {
                --level;
            }
            std::fprintf(fout, "%u", level % 10);
        }

        std::fputc('\n', fout);
        std::fprintf(fout, "Son shape:\n");
        std::fprintf(fout, "%s\n", son_shape);
    }

    const size_t limit = 10;
    size_t pos_found[limit];
    size_t count = FindFirstSubstrs(parent_shape, son_shape, pos_found, limit);
    if (count != 0) {
        std::fprintf(fout, "YES\n%u\n", count);

        size_t* levels = new size_t[count];
        // get level info
        size_t level = 1;
        size_t pos = 0;
        for (size_t index = 0; index < count; ++index) {
            while (pos != pos_found[index]) {
                if (parent_shape[pos] == '{') {
                    ++level;
                } else if (parent_shape[pos] == '}') {
                    --level;
                }
                ++pos;
            }
            levels[index] = level;

            //std::fprintf(fout, "%u\n", levels[index]);
        }

        // bubble sort
        for (size_t i = count - 1; i != 0; --i) {
            for (size_t j = 0; j < i; ++j) {
                if (levels[j] > levels[j + 1]) {
                    size_t temp = levels[j];
                    levels[j] = levels[j + 1];
                    levels[j + 1] = temp;
                }
            }
        }

        for (size_t i = 0; i < count; ++i) {
            fprintf(fout, "%u\n", levels[i]);
        }

    } else {
        std::fprintf(fout, "NO");
    }

    delete[] parent_shape;
    delete[] son_shape;
    fclose(fin_parent);
    fclose(fin_son);
    fclose(fout);

    return 0;
}
