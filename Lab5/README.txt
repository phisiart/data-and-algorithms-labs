This program is written in standard C++11, and has been tested with

1) Visual Studio 2013

2) (MinGW) g++ -std=c++11 main.cpp
Note that the "-std=c++11" flag cannot be ignored since there are "nullptr"s in the code.

The exe file I handed in is compiled with MinGW.