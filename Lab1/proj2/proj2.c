// Johnson Tan
// EE Tsinghua
// Homework 1.2 - Encode

#include <stdio.h>
#include <ctype.h>

int main(int argc, char* argv[]) {
    int ch;
    FILE* fin;
    FILE* fout;

    if (argc != 3) {
        printf("Usage: %s [input file name] [output file name]\n", argv[0]);
        return -1;
    }

    if ((fin = fopen(argv[1], "r")) == NULL) {
        printf("Error! Cannot open input file.");
        return -1;
    }

    if ((fout = fopen(argv[2], "w")) == NULL) {
        printf("Error! Cannot open output file.");
        fclose(fin);
        return -1;
    }

    while ((ch = fgetc(fin)) != EOF) {
        fputc(
            (isalnum(ch) ? ch + 5 : ch - 3),
            fout
        );
    }

    fclose(fin);
    fclose(fout);
    return 0;

}
