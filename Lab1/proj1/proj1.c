// Johnson Tan
// EE Tsinghua
// Homework 1.1 - Prime numbers

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_N 999

int main(int argc, char* argv[]) {
    int N;
    FILE* fp;
    int* primes;
    int index;
    int i;
    int num;
    int is_prime, found_prime;

    if (argc != 3) {
        printf("Usage: %s [N] [output file name]\n", argv[0]);
        return -1;
    }

    sscanf(argv[1], "%d", &N);
    if (N < 0 || N > MAX_N) {
        printf("Error! N must be between %d and %d\n", 0, MAX_N);
        return -1;
    }

    if ((fp = fopen(argv[2], "w")) == NULL) {
        printf("Error! Cannot open file.\n");
        return -1;
    }

    primes = (int*)malloc(sizeof(int) * N);

    num = 1;
    for (index = 0; index < N; ++index) {
        found_prime = 0;
        while (!found_prime) {
            ++num;

            is_prime = 1;
            // Is num a prime number?
            // If so, no prime number smaller than num divides it exactly
            // and is_prime is kept as 1
            for (i = 0; i < index; ++i) {
                if (num % primes[i] == 0) {
                    is_prime = 0;
                }
            }

            if (is_prime) {
                found_prime = 1;
            }

        }

        primes[index] = num;
    }

    for (i = 0; i < N; ++i) {
        fprintf(fp, "%d\n", primes[i]);
    }

    fclose(fp);
    return 0;
    
}
