﻿/*
*
*  Template for Experiment 7,
*  1)N_th Sorted Float
*  2)Solution For Quadratic Equation
*  3)Kahan Summation
*  4)ill-conditioned Summation
*  Pleas fill in the functions,do not modify other settings
*
*  2013/11
*
*/

#include <iostream>
#include <cmath>
#include <cstdio>
using namespace std;
#define LarN 100005      //the possible maximum length of input array,please do not alter it

float sorted_float(int N) {
    // N_th sorted normalized positive float number,the first one is +2^(-126)
    float ret;
    // your solution goes here
    int* pret = (int*)&ret;
    int iret;
    int sign = 0;
    if (N == 0) {
        return 0;
    }
    if (N < 0) {
        sign = 1;
        N = -N;
    }

    const int c = 0x800000;     // 2**23，也就是尾数的种数
    const int max = 0x7F000000; // 254 * c，当 N > max 时，为 infty
    int k;
    int r;
    int exp;
    if (N <= max) {
        // N 每增加 2**23，指数就增加 1

        k = (N - 1) / c; // 这个数就是指数 +126
        r = (N - 1) % c; // 这个数就是尾数

        exp = k + 1; // 这个数就是指数 +127，也就是 float 中表示指数的形式
    } else {
        exp = 255;
        r = 0;
    }

    iret = (sign << 31) + (exp << 23) + r;

    *pret = iret;
    return ret;
}

void quadratic_solution(double a, double b, double c, double* roots) {
    // solution for equation ax^2 + bx + c = 0
    // roots[0] = x1, roots[1] == x2, and x1 <= x2
    // your solution goes here

    // 有一个解是 0
    if (c == 0.0) {
        roots[0] = 0.0;
        roots[1] = -b / a;
        return;
    }

    if (a > b && a > c) {
        // max = a
        b /= a;
        c /= a;
        a = 1.0;
    } else if (b > a && b > c) {
        // max = b
        a /= b;
        c /= b;
        b = 1.0;
    } else {
        // max = c
        a /= c;
        b /= c;
        c = 1.0;
    }

    double delta = b * b - a * c * 4.0;
    if (delta < 0) {
        // no solution
        roots[0] = 0;
        roots[1] = 0;
        return;
    }
    double sqrt_delta = std::sqrt(delta);

    // consider x[0]
    if (b < 0) {
        roots[0] = c / (-b + sqrt_delta) * 2;
    } else {
        roots[0] = (-b - sqrt_delta) / 2 / a;
    }

    // consider x[1]
    if (b > 0) {
        roots[1] = c / (-b - sqrt_delta) * 2;
    } else {
        roots[1] = (-b + sqrt_delta) / 2 / a;
    }

    return;
}

double Kahan_sum(double* x, int N) {
    //summation of double array,N is the length of the array
    double ret;
    //your solution goes here
    double sum = 0.0;
    double c = 0.0;
    double y, t;
    for (size_t i = 0; i < N; ++i) {
        y = x[i] - c;
        t = sum + y;
        c = (t - sum) - y;
        sum = t;
    }
    return sum;

    return ret;
}

double ill_conditioned_sum(double* x, int N) {
    //summation of double array,N is the length of the array
    double ret = 0.0;
    //your solution goes here

    // 生命苦短，不写了


    return ret;
}

float n_inf = -1.0e100, p_inf = +1.0e100;

int main() {
    int T_case, N;
    double coefs[3], roots[2], x[LarN];
    float sf;
    //problem 1, sorted float number
    scanf("%d", &T_case);
    while (T_case--) {
        scanf("%d", &N);
        sf = sorted_float(N);
        if (sf == p_inf) {
            printf("+1.0e100\n");
        } else if (sf == n_inf) {
            printf("-1.0e100\n");
        } else {
            printf("%.9e\n", sf);
        }
    }
    //problem 2, solution for quadratic equation
    scanf("%d", &T_case);
    while (T_case--) {
        scanf("%le %le %le", &coefs[0], &coefs[1], &coefs[2]);
        quadratic_solution(coefs[0], coefs[1], coefs[2], roots);
        printf("%.16le %.16le\n", roots[0], roots[1]);
    }
    //problem 3, Kahan summation
    scanf("%d", &T_case);
    while (T_case--) {
        scanf("%d", &N);
        for (int i = 0; i < N; i++) {
            scanf("%X %X", (int*)(&x[i]) + 1, (int*)(&x[i]));
        }
        printf("%.16e\n", Kahan_sum(x, N));
    }

    //problem 4, ill-conditioned summation
    scanf("%d", &T_case);
    while (T_case--) {
        scanf("%d", &N);
        for (int i = 0; i < N; i++) {
            scanf("%X %X", (int*)(&x[i]) + 1, (int*)(&x[i]));
        }
        printf("%.16e\n", ill_conditioned_sum(x, N));
    }
    return 0;
}