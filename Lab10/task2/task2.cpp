﻿#include <cstdio>

const char X_WINS = 1;
const char O_WINS = -1;
const char STILL_NOBODY_WINS = 0;

const char O_WILL_DIE = -1;
const char O_WILL_LIVE = 0;

const char x_label = 'x';
const char o_label = 'o';
const char empty_label = '0';

const bool debug = false;
const bool print_score = false;

char empty_num;

char routes[8][3] = {
    { 1, 2, 3 },
    { 4, 5, 6 },
    { 7, 8, 9 },
    { 1, 4, 7 },
    { 2, 5, 8 },
    { 3, 6, 9 },
    { 1, 5, 9 },
    { 3, 5, 7 }
}; // Note that route is counted from 0 to 7. The mesh is counted from 1 to 9.

struct Board {
    Board() : this_move_side('n'), this_move_pos(0), empty_num(9) {}
    char this_move_side;        // 'x' or 'o' or 'n' for given
    char this_move_pos;         // 1 ~ 9 or 0 for given
    char data[10];              // 'x' or 'o' or empty_label, data[0] 留空
    char connectivity[8];       // 对于八条路径的连通性记录，'x' 的数量减去 'o' 的数量
    char empty_num;             // 棋盘上空余的位置数
    Board* next[9];             // 下一局棋盘
    float score;                // 当前棋盘的得分期望
};

void UpdateConnectivity(Board& board, size_t route);
void UpdateConnectivity(Board& board);
char Judge(Board& board);
Board* MakeMove(Board& board, char move_side, size_t pos, size_t index);
char Solve(Board& board);
void PrintBoard(Board& board);
void PrintConnectivity(Board& board);

// 被自动调用
void UpdateConnectivity(Board& board, size_t route) {
    if (route == -1) {
        return;
    }

    board.connectivity[route] = 0;
    for (size_t index = 0; index < 3; ++index) {
        if (board.data[routes[route][index]] == 'x') {
            ++board.connectivity[route];
        } else if (board.data[routes[route][index]] == 'o') {
            --board.connectivity[route];
        }
    }

}

// 确认正确
void UpdateConnectivity(Board& board) {
    char route1 = -1, route2 = -1, route3 = -1, route4 = -1;
    switch (board.this_move_pos) {
    case 1:
        route1 = 0;
        route2 = 3;
        route3 = 6;
        break;
    case 2:
        route1 = 0;
        route2 = 4;
        break;
    case 3:
        route1 = 0;
        route2 = 5;
        route3 = 7;
        break;
    case 4:
        route1 = 1;
        route2 = 3;
        break;
    case 5:
        route1 = 1;
        route2 = 4;
        route3 = 6;
        route4 = 7;
        break;
    case 6:
        route1 = 1;
        route2 = 5;
        break;
    case 7:
        route1 = 2;
        route2 = 3;
        route3 = 7;
        break;
    case 8:
        route1 = 2;
        route2 = 4;
        break;
    case 9:
        route1 = 2;
        route2 = 5;
        route3 = 6;
        break;
    }

    UpdateConnectivity(board, route1);
    UpdateConnectivity(board, route2);
    UpdateConnectivity(board, route3);
    UpdateConnectivity(board, route4);

}

char Judge(Board& board) {
    for (size_t index = 0; index < 8; ++index) {
        if (board.connectivity[index] == 3) {
            return X_WINS;
        } else if (board.connectivity[index] == -3) {
            return O_WINS;
        }
    }
    return STILL_NOBODY_WINS;
}

// 在某个位置落子，并创建子棋盘
Board* MakeMove(Board& board, char move_side, size_t pos, size_t index) {
    board.next[index] = new Board(board);
    board.next[index]->this_move_side = move_side;
    board.next[index]->this_move_pos = pos;
    board.next[index]->data[pos] = move_side;
    board.next[index]->empty_num--;
    UpdateConnectivity(*board.next[index]);

    if (debug) {
        PrintBoard(*board.next[index]);
    }
    return board.next[index];
}

// 1 ~ 9: 接下来由 'x' 落子，给出 'x' 的最优落子方法
// 其他
char Solve(Board& board) {

    // 本函数还要做清理内存的工作

    if (board.this_move_pos != 0) {
        UpdateConnectivity(board);
    }

    // 若该棋盘中，'x' 获胜了，显然 'o' 没活路
    if (Judge(board) == X_WINS) {
        return O_WILL_DIE;
    }

    if (Judge(board) == O_WINS) {
        return O_WILL_LIVE;
    }

    // 若该棋盘已被填满，但无人获胜，显然 'o' 有活路
    if (board.empty_num == 0) {
        return O_WILL_LIVE;
    }

    char move_side = (board.this_move_side == 'o') ? 'x' : 'o';

    if (move_side == 'x') {
        // 只要 'x' 有一种走法使得下一局棋盘上 'o' 没活路，那么这局棋盘就算 'o' 没活路。

        size_t index = 0;
        for (size_t pos = 1; pos <= 9; ++pos) {
            if (board.data[pos] == empty_label) {
                MakeMove(board, move_side, pos, index);

                if (Solve(*board.next[index]) == O_WILL_DIE) {

                    // 清理内存
                    for (size_t i = 0; i <= index; ++i) {
                        delete board.next[i];
                    }
                    return O_WILL_DIE;
                }
                ++index;
            }
        }

        for (size_t i = 0; i < board.empty_num; ++i) {
            delete board.next[i];
        }
        return O_WILL_LIVE;

    } else if (move_side == 'o') {
        // 只要 'o' 有一种走法使得下一局棋盘上 'o' 有活路，那么这局棋盘就算 'o' 有活路，并返回一种走法。

        size_t index = 0;
        for (size_t pos = 1; pos <= 9; ++pos) {
            if (board.data[pos] == empty_label) {
                MakeMove(board, move_side, pos, index);

                if (Solve(*(board.next[index])) != O_WILL_DIE) {
                    for (size_t i = 0; i <= index; ++i) {
                        delete board.next[i];
                    }
                    return pos;
                }
                ++index;
            }
        }

        for (size_t i = 0; i < board.empty_num; ++i) {
            delete board.next[i];
        }
        return O_WILL_DIE;

    }

}

char GetScore(Board& board, float score_for_tie) {
    if (board.this_move_pos != 0) {
        UpdateConnectivity(board);
    }

    char judge = Judge(board);
    if (judge == X_WINS) {
        board.score = 1.0f;
        return 0;
    } else if (judge == O_WINS) {
        board.score = 0.0f;
        return 0;
    }
    if (board.empty_num == 0) {
        board.score = score_for_tie;
        return 0;
    }

    char move_side = (board.this_move_side == 'x') ? 'o' : 'x';

    if (move_side == 'x') {
        // 我们必须找一种得分期望最大的走法

        size_t board_index = 0;
        float max_score = -1.0f;
        char best_choice;
        for (size_t pos = 1; pos <= 9; ++pos) {
            if (board.data[pos] == empty_label) {
                MakeMove(board, x_label, pos, board_index);
                GetScore(*board.next[board_index], score_for_tie);
                if (board.next[board_index]->score > max_score) {
                    best_choice = pos;
                    max_score = board.next[board_index]->score;
                }
                ++board_index;
            }
        }
        board.score = max_score;

        if (print_score) {
            if (empty_num == board.empty_num) {
                std::printf("Empty num = %d Score = %f\n", board.empty_num, board.score);
            }
        }

        return best_choice;

    } else {
        // 我们要等概率落子并求得平均得分期望

        size_t board_index = 0;
        float score_sum = 0.0f;
        for (size_t pos = 1; pos <= 9; ++pos) {
            if (board.data[pos] == empty_label) {
                MakeMove(board, o_label, pos, board_index);
                GetScore(*board.next[board_index], score_for_tie);
                score_sum += board.next[board_index]->score;
                ++board_index;
            }
        }
        board.score = score_sum / board.empty_num;

        if (print_score) {
            if (empty_num - 1 == board.empty_num) {
                std::printf("Empty num = %d Score = %f\n", board.empty_num, board.score);
            }
        }

        return 0;
    }
}

void PrintBoard(Board& board) {
    std::printf("+-+-+-+\n");
    std::printf("|%c|%c|%c|\n", board.data[1], board.data[2], board.data[3]);
    std::printf("+-+-+-+\n");
    std::printf("|%c|%c|%c|\n", board.data[4], board.data[5], board.data[6]);
    std::printf("+-+-+-+\n");
    std::printf("|%c|%c|%c|\n", board.data[7], board.data[8], board.data[9]);
    std::printf("+-+-+-+\n\n");
    /*std::printf("%c %c %c\n%c %c %c\n%c %c %c\n\n",
    board.data[1],
    board.data[2],
    board.data[3],
    board.data[4],
    board.data[5],
    board.data[6],
    board.data[7],
    board.data[8],
    board.data[9]
    );*/
}

void PrintConnectivity(Board& board) {
    for (size_t index = 0; index < 8; ++index) {
        std::printf("%d\t", board.connectivity[index]);
    }
}

int main(int argc, char* argv[]) {
    if (argc != 11) {
        std::printf("Wrong input!\n");
        return -1;
    }

    // Board board{ 'n', 0, { 0, empty_label, 'x', 'o', 'x', empty_label, empty_label, empty_label, empty_label, empty_label }, { 0, 0, 0, 0, 0, 0, 0, 0 }, 5 };
    Board board;
    //size_t x_count = 0, o_count = 0;
    for (size_t pos = 1; pos <= 9; ++pos) {
        //std::scanf("%c ", &board.data[pos]);
        board.data[pos] = argv[pos][0];
        if (board.data[pos] != empty_label) {
            --board.empty_num;
        }
    }
    //UpdateConnectivity

    empty_num = board.empty_num;

    float score_for_tie;
    std::sscanf(argv[10], "%f", &score_for_tie);
    if (debug) {
        PrintBoard(board);
    }
    std::printf("%d\n", GetScore(board, score_for_tie));

    //std::printf("%d", sizeof(board));

    return 0;
}
