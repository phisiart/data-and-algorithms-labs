#include <cstdio>
#include <cstring>

bool debug = false;

size_t LIS(const int X[], size_t N, size_t endpos[], size_t prev[]) {

    // endpos[0] = N
    // prev[N] = N

    for (size_t n = 0; n < N + 1; ++n) {
        endpos[n] = N;
        prev[n] = N;
    }
    endpos[0] = N;
    prev[N] = N;

    size_t L = 0;

    for (size_t n = 0; n < N; ++n) {

        // search for the biggest len such that X[endpos[len]] < X[n]
        size_t maxlen = 0;

        size_t left = 1, right = n;

        while (1) {
            if (left == right) {
                if (endpos[right] != N && X[endpos[right]] < X[n]) {
                    maxlen = left;
                }
                break;
            } else if (left > right) {
                maxlen = 0;
                break;
            }
            size_t middle = (left + right + 1) / 2;
            if (endpos[middle] != N && X[endpos[middle]] < X[n]) {
                left = middle;
            } else {
                right = middle - 1;
            }
        }
        
        prev[n] = endpos[maxlen];

        if (endpos[maxlen + 1] == N || X[endpos[maxlen + 1]] > X[n])
            endpos[maxlen + 1] = n;
        
        if (maxlen == L) {
            L = maxlen + 1;
        }
        
    }
    
    if (debug) {
        std::printf("n     : ");
        for (size_t n = 0; n < N; ++n) {
            std::printf("%4d", n);
        }

        std::printf("\nX     : ");
        for (size_t n = 0; n < N; ++n) {
            std::printf("%4d", X[n]);
        }
        std::printf("\nEndpos: ");
        for (size_t n = 0; n <= N; ++n) {
            std::printf("%4d", endpos[n]);
        }
        std::printf("\nPrev  : ");
        for (size_t n = 0; n <= N; ++n) {
            std::printf("%4d", prev[n]);
        }
        std::printf("\n");
    }

    return L;
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::printf("Usage: %s [input] [output]\n");
        return -1;
    }

    std::FILE* fin;
    if ((fin = std::fopen(argv[1], "r")) == NULL) {
        std::printf("Cannot open file.");
        return -1;
    }

    int N;
    std::fscanf(fin, "%d", &N);

    std::FILE* fout;
    if ((fout = std::fopen(argv[2], "w")) == NULL) {
        std::printf("Cannot open file.");
        return -1;
    }

    int* X = new int[N];
    for (size_t n = 0; n < N; ++n) {
        std::fscanf(fin, "%d", &X[n]);
    }

    size_t* endpos = new size_t[N + 1];
    size_t* prev = new size_t[N + 1];
    size_t L = LIS(X, N, endpos, prev);

    std::fprintf(fout, "%u\n", L);

    int* lis = new int[L];
    size_t pos = endpos[L];
    for (int index = L - 1; index >= 0; --index) {
        lis[index] = X[pos];
        pos = prev[pos];
    }

    for (size_t index = 0; index < L; ++index) {
        std::fprintf(fout, "%d ", lis[index]);
    }
    std::fprintf(fout, "\n");

    delete[] lis;

    std::fclose(fin);
    std::fclose(fout);
    return 0;

}